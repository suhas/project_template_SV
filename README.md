# Project

Data created:
`Last updated: `

# Project description

# People

**Creator** - Suhas Vijayakumar, @suhas

**Supervisors** - Rogier Mars, @neuroecology | Pieter Medendorp

**Collaborators** -


# Institute

Donders Institute for Brain, Cognition and Behaviour, Nijmegen, NL

Donders Centre for Cognition, Nijmegen, NL

# Directory structure
```
.
├── README.md
├── code
├── data
├── docs
│   ├── article
│   ├── notes
│   │   └── article_summaries.md
│   └── presentation
├── logs
│   └── meeting_notes.md
├── results
│   └── figures
└── scratch
```

# Links

# Current status

 - [x] Setup GitLab project directory
 - [ ] 
